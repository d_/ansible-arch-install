#!/usr/bin/env python

from argparse import ArgumentParser
from getpass import getpass
from os import environ
import subprocess


def execute(cmd, env):
    popen = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, universal_newlines=True, env=env
    )
    for stdout_line in popen.stdout:
        print(stdout_line, end="")
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)


def askpass(prompt):
    p0, p1 = 0, 1
    while p0 != p1:
        p0 = getpass(f"{prompt}: ")
        p1 = getpass(f"{prompt} (confirm): ")
    return p0


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("address")
    args = parser.parse_args()

    luks_pass = askpass("LUKS Password")
    user_pass = askpass("User Password")
    hostname = input("Hostname (e.g. bill): ")
    drivename = input("Drive Name (e.g. /dev/nvme0n1): ")

    my_env = environ.copy()
    my_env["ANSIBLE_FORCE_COLOR"] = "1"
    execute(
        [
            "ansible-playbook",
            "run.yml",
            "-k",
            "-i",
            f"root@{args.address},",
            "-e",
            f"luks_pass={luks_pass} user_pass={user_pass} hostname={hostname} drivename={drivename}",
        ],
        env=my_env,
    )
