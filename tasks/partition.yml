---
- name: Create ESP
  parted:
    device: '{{ drivename }}'
    label: gpt
    number: 1
    part_end: 512MiB
    name: boot
    flags: [boot,esp]
    state: present
- name: Create part to encrypt and contain LVM
  parted:
    device: '{{ drivename }}'
    label: gpt
    number: 2
    part_start: 512MiB
    name: cryptlvm
    state: present

# https://unix.stackexchange.com/questions/500887/given-a-block-device-how-to-detect-if-names-of-partitions-must-contain-p
- name: Deduce block device name of first partition
  set_fact:
    part1: '{{ ( drivename[-1] | int != 0 ) | ternary( drivename + "p1", drivename + "1" ) }}'
- name: Deduce block device name of second partition
  set_fact:
    part2: '{{ ( drivename[-1] | int != 0 ) | ternary( drivename + "p2", drivename + "2" ) }}'

- name: create and open LUKS container
  luks_device:
    device: "{{ part2 }}"
    state: opened
    name: "cryptlvm"
    passphrase: "{{ luks_pass }}"

- name: Create volume group `vg` on top of /dev/mapper/cryptlvm
  lvg:
    pvs: /dev/mapper/cryptlvm
    vg: vg
    state: present

- name: Create 16G swap logical volume within `vg`
  lvol:
    vg: vg
    lv: swap
    size: 16G
    state: present
    active: yes

- name: Create 100%FREE root logical volume within `vg`
  lvol:
    vg: vg
    lv: root
    size: 100%FREE
    shrink: no
    state: present
    active: yes

- name: Format esp
  filesystem:
    dev: "{{ part1 }}"
    fstype: vfat
    opts: "-n esp"
    state: present

- name: Format swap
  filesystem:
    dev: "/dev/mapper/vg-swap"
    fstype: swap
    opts: "-L swap"
    state: present

- name: Format root
  filesystem:
    dev: "/dev/mapper/vg-root"
    fstype: btrfs
    opts: "-L root"
    state: present

- name: Mount root volume to /mnt
  mount:
    path: /mnt
    src: /dev/mapper/vg-root
    fstype: btrfs
    opts: "noatime,compress=zstd"
    state: mounted

- name: swapon swap volume
  when: ansible_swaptotal_mb < 1
  shell: "swapon /dev/mapper/vg-swap"

- name: mkdir /mnt/boot and mount esp to it
  block:
    - name: mkdir /mnt/boot
      file:
        path: /mnt/boot
        state: directory
        mode: "0755"
    - name: mount esp to /mnt/boot
      mount:
        path: /mnt/boot
        src: "{{ part1 }}"
        fstype: vfat
        state: mounted
